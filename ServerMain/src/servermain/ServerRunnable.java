/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servermain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author RENT
 */
public class ServerRunnable implements Runnable {

    private IMessageListener listener;
    private BufferedReader reader;
    private PrintWriter writer;

    public ServerRunnable(IMessageListener listener) {
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            ServerSocket socketPolaczenia = new ServerSocket(10001);
            while (true) {

                Socket otwartySocket = socketPolaczenia.accept();

                //otwarcie wejścia
                InputStream kanalWejscia = otwartySocket.getInputStream();
                reader = new BufferedReader(new InputStreamReader(kanalWejscia));

                //otwarcie wyjścia
                OutputStream kanalWyjscia = otwartySocket.getOutputStream();
                writer = new PrintWriter(kanalWyjscia);

                String adresIp = otwartySocket.getInetAddress().getHostAddress();
                String otrzymanaLinia = null;
                do {
                    otrzymanaLinia = reader.readLine();
                    listener.messageReceived(otrzymanaLinia, adresIp);
                } while (otrzymanaLinia != null);

                reader.close();
                writer.close();
            }
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
            ioe.printStackTrace();
        }
    }

}
