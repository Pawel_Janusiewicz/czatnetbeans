$(document).ready(function(){
    
    $(".dropdown-toggle").dropdown();
    
    $("#animateButton").click(function(){
        var div = $("#animationDot1");
        var div2 = $("#animationDot2");
        var div3 = $("#animationDot3");
        var div4 = $("#animationDot4");
        var div5 = $("#animationDot5");
        var div6 = $("#animationDot6");
        var div7 = $("#animationDot7");
        var div8 = $("#animationDot8");
        var div9 = $("#animationDot9");
        var div10 = $("#animationDot10");
        var div11 = $("#animationDot11");
        
        div.animate({"top": '+=50px'}, "fast");
        div.animate({"top": '-=50px'}, "fast");
        
        div2.delay(100).animate({"top": '+=50px'}, "fast");
        div2.animate({"top": '-=50px'}, "fast");
        
        div3.delay(200).animate({"top": '+=50px'}, "fast");
        div3.animate({"top": '-=50px'}, "fast");
        
        div4.delay(300).animate({"top": '+=50px'}, "fast");
        div4.animate({"top": '-=50px'}, "fast");
        
        div5.delay(400).animate({"top": '+=50px'}, "fast");
        div5.animate({"top": '-=50px'}, "fast");
        
        div6.delay(500).animate({"top": '+=50px'}, "fast");
        div6.animate({"top": '-=50px'}, "fast");
        
        div7.delay(600).animate({"top": '+=50px'}, "fast");
        div7.animate({"top": '-=50px'}, "fast");
        
        div8.delay(700).animate({"top": '+=50px'}, "fast");
        div8.animate({"top": '-=50px'}, "fast");
        
        div9.delay(800).animate({"top": '+=50px'}, "fast");
        div9.animate({"top": '-=50px'}, "fast");
        
        div10.delay(900).animate({"top": '+=50px'}, "fast");
        div10.animate({"top": '-=50px'}, "fast");
        
        div11.delay(1000).animate({"top": '+=50px'}, "fast");
        div11.animate({"top": '-=50px'}, "fast");
    });
});